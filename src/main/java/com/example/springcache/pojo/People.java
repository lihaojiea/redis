package com.example.springcache.pojo;

import java.io.Serializable;

//实体类必须序列化  要不然不能进行缓存
public class People  {

    private Long id;
    private String name;

    public People() {
    }

    public People(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
