package com.example.springcache.service;

import com.example.springcache.pojo.People;

public interface DemoService {
    String demo();

    String selectById(long id);

    //修改操作 如果没有缓存则新增 如果有则修改
    String selectByIdUpdate(long id);

    People selectByUsername(long id,String name);

    People selectById2(long id);

    //删除缓存
    void delete();
}
