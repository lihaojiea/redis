package com.example.springcache.controller;

import com.example.springcache.pojo.People;
import com.example.springcache.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Autowired
    private DemoService demoService;

    @RequestMapping("/demo")
    public String demo(){
        return demoService.demo();
    }

    @RequestMapping("/selectById")
    public String selectById(long id){
        return demoService.selectById(id);
    }

    @RequestMapping("/selectByUsername")
    public People selectByUsername(long id,String name){
        return demoService.selectByUsername(id, name);
    }

    @RequestMapping("/selectById2")
    public People selectById2(long id){
        return demoService.selectById2(id);
    }

    @RequestMapping("/delete")
    public String delete(){
        demoService.delete();
        return "delete方法";
    }

    @RequestMapping("/Update")
    public String Update(long id){

        return demoService.selectByIdUpdate(id);
    }
}
